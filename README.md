# Old English Core Vocabulary 

Derived from [a list of 500+ Old English words](https://www.st-andrews.ac.uk/~cr30/vocabulary/) compiled by Christine Rauer, University of St Andrews.
